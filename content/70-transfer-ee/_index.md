+++
title = "Transfer Execution Environment"
weight = 70
+++

## Transfer your Execution Environment to private automation hub

By now you have learned how to use a collection that is not included in one of the official EE's. Then you built a custom EE with the required collection and tested it.

The next step is to get your custom EE into a container registry so it can be used in automation controller. And since PAH provides a container registry, too, we'll use it.

### Prerequisites

* The (working) custom execution environment from the previous chapter
* Access to your private automation hub to store your EE

### Tasks

* Push your EE to your private automation hub
* Add additional tags or labels and push them to the registry
* Delete old versions of the EE

### Push image to PAH

Pushing an EE to the PAH registry is pretty straight forward: You just use Podman like with any other registry. The parameters you need are:

* The registry name, this is the same as the hostname of your private automation hub
* Username/password, again same as for the web UI

Then do the following:

* Find the name of the local image
* Tag the local image with the registry
* Push it

The commands you have to run are:

```bash
export student_name=# the student name you provided when ordering the environment
podman login hub-$student_name.learnfest.ansible-labs.de
podman images
podman tag localhost/ee-ansible-demo:0.1.0  hub-$student_name.learnfest.ansible-labs.de/ee-ansible-demo:latest
podman push hub-$student_name.learnfest.ansible-labs.de/ee-ansible-demo
```

Check the image is in PAH:

* log into the Web UI of your private automation hub
* you should find the execution environment in the **Execution Environments** menu.

{{< figure src="../images/pah-ee.png?width=50pc&classes=border,shadow" title="Click image to enlarge" >}}

### Goals

* Publish an EE in your own registry
* Get familiar with basic EE management tasks

**You are now ready to use your custom Execution Environment in automation controller.**

### Tips

* [Skopeo documentation](https://github.com/containers/skopeo)
* [Podman documentation](https://docs.podman.io/en/latest/)
