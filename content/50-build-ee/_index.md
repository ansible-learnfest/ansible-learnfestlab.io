+++
title = "Build an execution environment"
weight = 50
+++

## Build an execution environment

Execution environments are Linux container images which are built on top of the [Red Hat Universal Base Images](https://www.redhat.com/en/blog/introducing-red-hat-universal-base-image) (UBI) and additionally contain:

* ansible-core
* Python and its dependencies
* Ansible collections and their dependencies
* optionally additional software packages like RPMs

This makes the execution of an Ansible Playbook more scalable, reliable and predictable since the combination of Playbook and execution environment should always deliver the same results.

### Prerequisites

For this chapter you need the tools `ansible-builder` and `ansible-navigator` in your lab environment. If you use the VS Code Server provided by this lab, **they both have been already installed for you**, the instructions below are just for informational purposes.

{{% notice note %}}
`ansible-builder` is a high-level tool for building Execution Environments that abstracts away a lot of the intricacies of container image building. Under the hood it uses `podman`, of course.
{{% /notice %}}

{{% notice note %}}
On RHEL they are provided by the AAP repo, on Fedora you will need `pip install ansible-builder`, it’s recommended to use a [Python virtual environment](https://docs.python.org/3/library/venv.html) in this case.
{{% /notice %}}

```bash
# Only if you're not on RHEL
dnf -y install python3-virtualenv
virtualenv ansible-builder
. ansible-builder/bin/activate
pip install -U pip ansible-builder
# If you are on RHEL, enable the repository, if not already done
subscription-manager config --rhsm.manage_repos 1
subscription-manager repos --enable=ansible-automation-platform-2.3-for-rhel-9-x86_64-rpms
yum install ansible-builder
```

### Tasks

In the previous chapter you have learned how you can still use collections not contained in an Execution Environment in a Playbook. But in many cases you'll start building custom EE's at some point containing collections you use frequently in your Ansible content.

In this chapter you'll do exactly this: build a custom Execution Environment.

#### Check out repo

We have prepared a repository with the needed content to build the EE image. Go and clone the repo to your VS Code terminal:

```bash
git clone https://github.com/ansible-learnfest/ee-flow.git
```

#### Login to registry.redhat.io

As the base image will be pulled from the Red hat container registry, you have to login with your personal Red Hat login credentials (the one you use on [access.redhat.com](https://access.redhat.com)). In the VS Code terminal enter the following commands. The `loginctl` command is needed to make `podman` work in the VS Code Server terminal.

```bash
sudo loginctl enable-linger 1000
podman login registry.redhat.io
```

#### Build Execution Environment

* Change to the `ee-flow/ansible-builder/` directory
* Examine the three files that describe your execution environment (these are all in the default state listing nothing so far):
  * `bindep.txt` lists packages like RPMs or dpkg, that need to be installed into the EE
  * `requirements.txt` is for installing additional Python dependencies
  * `requirements.yml` might be the most important one and lists the Ansible Collections which will be installed into the EE

{{% notice note %}}
The three files describing the content and possible additional build steps are pulled together in a `.yml` build file, `ee-ansible-demo.yml` in our case. Again feel free to examine the file.
{{% /notice %}}

* We need the `containers.podman` collection in the EE, change the `requirements.yml` to include it (hint: use the content of the file you uploaded to PAH in the previous chapter).
* That's all. Run `ansible-builder` to create the new EE as in the example below (make sure you are in `ee-flow/ansible-builder/`):

```bash
ansible-builder build -f ee-ansible-demo.yml -t ee-ansible-demo:0.1.0 -v 3
```

{{% notice note %}}
We're using the `-v 3` flags to get more detailed output from `ansible-builder` - by default the tool is very quiet. Also note how we set a tag for the image.
{{% /notice %}}

* After the build has finished, check the image is there:

```bash
podman images
```

As `podman` is used to actually build the image, the `Containerfile` needed by Podman is created by `ansible-builder`. Please take the time to locate it and have a look at it.

### Goals

* Build an EE adding at least one collection.

### Tips

* [Ansible-builder documentation](https://access.redhat.com/documentation/en-us/red_hat_ansible_automation_platform/2.3/html/ansible_builder_guide)
* [Ansible Builder upstream documentation](https://ansible-builder.readthedocs.io/en/stable/index.html)
* [Ansible Builder upstream documentation](https://ansible-navigator.readthedocs.io/en/latest/)
* [List of certified collections](https://access.redhat.com/articles/3642632)
* [Simple EE example](https://gitlab.com/cjung/ansible-ee-intro)
