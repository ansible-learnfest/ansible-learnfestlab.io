+++
title = "Use an execution environment"
weight = 60
+++
## Use the Execution Environment

In this chapter we will use the execution environment on the command line.

### Prerequisites

You should have built your execution environment `ee-ansible-demo` - if not, please go back to the previous chapter.

### Tasks

Before we push a custom EE to a registry and use it in automation controller we want to make sure it provides what we need to run our Playbooks with all dependencies. Basically check if it works... :-)

For this we run a Playbook in the runtime environment the EE provides. Because `ansible-playbook` can't do this, we need to use the second new tool on the block, `ansible-navigator`:

* Configure `ansible-navigator` to use the previously created EE either by
  * specifying it on the command line
  * or by creating an `.ansible-navigator.yml` configuration file in your home directory
* To specify the EE image on the command line, look for `Specify the name of the execution environment image` in the output of `ansible-navigator --help`.
* Or if you prefer to use a config file, create a `~/.ansible-navigator.yaml` dot-file and add this content:

```yaml
---
ansible-navigator:
  execution-environment:
    image: ee-ansible-demo:0.1.0
```

* After pointing to our new EE, run `ansible-navigator` and start to explore its features. Navigating is straight forward, first check the `ee-ansible-demo` EE contains the `containers.podman` collection:
  * Get a list of included collections with `:collections` on the start page. `ESC` always takes you back one step in the menu structure.

Then have a look at the included EE images:

* Inspect the image with `:images`, choose an image by typing the row number
* Just look around and remember `ESC` is your friend when you got lost in menus items...
* Exit `ansible-navigator`

Now we finally want to run a Playbook to test the new EE. The demo Playbook you used before in automation controller is part of the GitHub repo `ee-flow` you checked out already. So we just need an inventory to go with it.

* Create the inventory file `~/ee-flow/ansible-builder/lab_inventory.ini`. The result should look something like this (get the FQDN from the dynamic inventory in automation controller):

```ini
[managed_nodes]
node2 ansible_host=<node 2 FQDN> ansible_user=ec2-user
```

{{% notice note %}}
You can find the FQDN of node 2 in **Hosts** menu of automation controller and the "public_dns_name" attribute of the host.
{{% /notice %}}

* Start `ansible-navigator` and run the Playbook:
  * `:run ~/ee-flow/deploy-container.yml -i ~/ee-flow/ansible-builder/lab_inventory.ini`
  * While it's running enter the line number for the play `deploy httpd container`, this will bring you to the task list from where you can inspect the running tasks.
  * After the run has finished, leave `ansible-navigator` with multiple `ESC`, or if you're a vi fan with ':q'.
  * Check the deployment worked:

```bash
# you can find the FQDN of the instance in your automation controller in the **Hosts** menu
$ curl <node 2 FQDN>
Welcome to Ansible LearnFest!
```

There is a command line parameter in `ansible-navigator` that makes the run and output mimic `ansible-playbook`, and is not jumping into the TUI interface. Give it a try:

```bash
ansible-navigator run ~/ee-flow/deploy-container.yml -i ~/ee-flow/ansible-builder/lab_inventory.ini -m stdout
```

### Goals

* Use `ansible-navigator` to inspect and run a Playbook in a custom EE.

### Tips

* [Ansible-navigator documentation](https://access.redhat.com/documentation/en-us/red_hat_ansible_automation_platform/2.3/html-single/ansible_navigator_creator_guide/index)
* [List of certified collections](https://access.redhat.com/articles/3642632)
* [Simple EE example](https://gitlab.com/cjung/ansible-ee-intro)
