# Ansible LearnFest Guide

## What is the Ansible LearnFest?

The **Ansible LearnFest** is a format we created because we felt there is a gap between the enablement and training offerings from Red Hat (workshops, guided labs, official trainings) and "real" hackathons with an open agenda. Our goal is to create a format where we can **learn, discuss and get hands-on experience**.

An Ansible LearnFest will usually be split into two parts:

* first half day with presentations, Q&A sessions and discussions
* second half day hands-on lab with AAP

## Collaboration

The Ansible LearnFest and this guide was created by Goetz Rieger and Christian Jung.
Feel free to create a merge request or an issue in [GitLab](https://gitlab.com/ansible-learnfest)
