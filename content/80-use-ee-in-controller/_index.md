+++
title = "Use the EE in automation controller"
weight = 80
+++

## Use the EE in automation controller

You have tested running a Playbook that depends on the Podman collection with `ansible-navigator` in your custom Execution Environment. Now it's time to use the custom EE with the Podman collection in automation controller.

### Prerequisites

* The custom EE was pushed and is published in PAH

### Tasks

* Check automation controller is configured to use EE's in PAH
* Use the custom EE in automation controller

### Configure EE in automation controller

PAH should be configured correctly in your lab environment by the initial installation, just check to learn where it's done:

* Check the **Credential** `Automation Hub Container Registry` points to PAH
  * **Name**: Automation Hub Container Registry
  * **Organization**: Default
  * **Credential Type**: Container Registry
  * **Authentication URL**:hub-&lt;student name&gt;.learnfest.ansible-labs.de
  * **Username**: admin
  * **Password**: &lt;your secret password>

{{% notice note %}}
You can find the Authentication URL on your private automation hub by navigating to **Execution Environments**, select the EE you want to use and then in **Details**.
{{% /notice %}}

* Go to `Execution Environments` and add the new EE
  * **Name**: ee-ansible-demo
  * **Image**: hub-&lt;student name&gt;.learnfest.ansible-labs.de/ee-ansible-demo
  * **Pull**: Always
  * **Organization**: Default
  * **Registry Credential**: Automation Hub Container Registry
* Click **Save**

{{% notice note %}}
Get the image location from your PAH: In **Execution Environments** click on the image and you will see an example command to pull the image which has the image name included.
{{% /notice %}}

### Bring it all together

This time we'll run the Playbook you used before, but in your custom EE! That means we shouldn't have to use a `requirements.yml` file to automatically load the needed collection because `containers.podman` is already included in the EE.

* Create a new **Project** using `https://github.com/ansible-learnfest/ee-flow.git` **with the branch `wo-requirements`**. This branch contains no `requirement.yml` to automatically add the collection. If you want to double check, have a look a the Job output and you'll notice that no collection was installed during project sync.
* Create a **Job Template** with the same settings as in the `Using private automation hub` chapter, or just create a copy of it. We want to make one change in the copied job template:
  * Set the **Execution Environment** to `ee-ansible-demo`
  * Set the **Project** to the one you just created (without requirements.yml)
  * Set **Limit** to `node3`
* Run the Job Template and check the outcome

### Goal

* Get familiar with using EE's in controller and different job templates

### Tips

You can disable automation controller's feature to automatically download and install Ansible Collections by navigating to **Settings** -> **Job Settings**. Here you can disable `Enable Collection(s) Download` and `Enable Role Download`. By doing so, you force the automation controller to only use collections which are already part of the execution environment. This can be useful to make sure the collection inside the EE is not overridden by a newer version provided by your private automation hub or Ansible Galaxy. This gives you more predictable and reproducible Ansible Playbook execution.
