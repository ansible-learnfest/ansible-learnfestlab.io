+++
title = "Final Challenge"
weight = 90
+++

### And finally: The Challenge

Now that you have learned how all the stages of building, testing, pushing and using a custom Execution Environment work, do it without help from the guide. Here are your tasks:

* The Playbook `enforce-selinux.yml` in `https://gitlab.com/ansible-learnfest/playbook-challenge.git` should be run with your custom EE against your managed nodes.
* It uses the collection `redhat.rhel_system_roles` that **is not part of your EE** yet.

So this is what you have to do:

* Add the needed collection to your EE definition and build a new version.
* Because the collection comes from Red Hat Automation Hub and not Ansible Galaxy, you need to create a `ansible.cfg` like this (fill in your AH token):

```ini
[galaxy]
server_list=rh-certified,published,community

[galaxy_server.rh-certified]
url=https://hub-<student name>.learnfest.ansible-labs.de/api/galaxy/content/rh-certified/
token=<your token>

[galaxy_server.published]
url=https://hub-<student name>.learnfest.ansible-labs.de/api/galaxy/content/published/
token=<your token>

[galaxy_server.community]
url=https://hub-<student name>.learnfest.ansible-labs.de/api/galaxy/content/community/
token=<your token>
```

You can find the URL for each repository in your private automation hub in the tab **Repository Management** and you can create an API token under **API token management**. Double check you did actually synchronize the 'redhat.rhel_system_roles' collection to your private automation hub!

* Tell `ansible-builder` in the definition file to read the `ansible.cfg` file:

```yaml
[...]
dependencies:
  galaxy: requirements.yml
  python: requirements.txt
  system: bindep.txt

ansible_config: /path/to/ansible.cfg
[...]
```

* Add `redhat.rhel_system_roles` to your EE build definition (`requirements.yml`) and build a new version.
* Using `ansible-navigator` inspect the new image to contain the needed collection
* You can test the new EE with `ansible-navigator` like you did in the "Build an execution environment" chapter
  * You have to clone the Github repo with the Playbook then!
* Push the new image to your PAH
* Create a **Project** pointing to `https://gitlab.com/ansible-learnfest/playbook-challenge.git`
* Create a **Job Template** using the new **Project** with the `enforce-selinux.yml` Playbook and the new version of the EE
* Run it!
